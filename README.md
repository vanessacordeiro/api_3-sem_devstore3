# Sprint 3

<img src="/Imagens_Sprint3/logo.jpg"/>

## :computer: Proposta

Na Sprint 3,realizamos a montagem e planejamento de votação Planning Pokker,como tambem a criação do sistema de Chat para melhor comunicação dos usario durante a votação e salas para a realização para votação. Alem disso com um sistema de envio de E-mail atraves do Front-end para realizar convites para prticipação de atividades do projeto e por ultimo a tela de restrospectiva de controle e votação das atividades e sua respectiva listagem.


## :dart: Detalhes da Sprint:

**Implementações do back-end:**

- Criação de salas de votação no Planning Pokker.
- Votação do sistema Planning Pokker.
- inserção do sistema de chat para a melhor comunicação no sistema Planning Pokker.


**Implementações Front-End:**
-  tela de Restrospectiva inserida no projeto e listagem das atividades inseridas no sistema.
- inserção de sistema de envio de E-mail através do front end.



## Apresentação da Sprint
- Em desenvolvimento.



## Wireframes:

- Em desenvolvimento



## :department_store: Proposta para a Proxima Sprint:
- Para a próxima sprint teremos o objetivo de desenvolver o sistema de resultado de votação realizado no em uma sala do sistema, quem participou da votação do sistema e inserção de itens para realizar sua avaliação e votação no sistema.

